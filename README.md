# README #


### API Calls ###

 * /api/cars -         Returns all cars
 * /api/new-car/ -     Creates a new car
 * /api/update-car/ -  Updates a car
 * /api/delete-car/ -  Deletes a car
 * /api/new-car-report/1 -  Car Stock Created Today
 * /api/new-car-report/2-  Car Stock Created Current Week
 * /api/new-car-report/3-  Car Stock Created Current Month
 * /api/car-archive-report/4 - Car Archived Today
 * /api/car-archive-report/5 - Car archived Current Week
 * /api/car-archive-report/6 - Car Archived Current Month
 * /api/search-cars/{keyword} - Search Cars or Dealers
 
 * /api/dealers/              - Returns all Dealers
 * /api/new-dealer/           - Create a new Dealer
 * /api/update-dealer/        - Update a Dealer
 * /api/delete-dealer         - Delete a Dealer
