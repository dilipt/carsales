﻿using CarStockManagement.Constants;
using CarStockManagement.Models.Domain;
using CarStockManagement.Service.DataService;
using CarStockManagement.Service.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarStockManagement.Service.Service
{
    public class CarService : ICarService
    {
        private CarDataService _carDataService;
        private EmailService _emailService;

        public CarService(CarDataService carDataService, EmailService emailService)
        {
            _carDataService = carDataService;
            _emailService = emailService;
        }

        public IEnumerable<Car> GetAllCars()
        {
            try
            {

                var cars = _carDataService.GetAllCars();

                return cars;
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public string CreateCar(Car car)
        {
            try
            {
                car.DateCreated = DateTime.Now;
                car.DateModified = DateTime.Now;
                
                string model = _carDataService.CreateCar(car);
                return model;
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public bool UpdateCar(Car car)
        {
            try
            {
               return _carDataService.UpdateCar(car);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public bool DeleteCar(Car car)
        {
            try
            {
                return _carDataService.DeleteCar(car);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public string NewCarsReport(int flag)

        {
            try
            {
                List<Car> cars = GetAllCars().ToList();
                if (flag == EmailConstant.EMAIL_CARS_CREATED_TODAY)
                {
                    cars = cars.Where(x => x.DateCreated.Date == DateTime.Today).ToList();
                    return _emailService.ProcessEmail(EmailConstant.EMAIL_CARS_CREATED_TODAY, cars);
                }
                else if (flag == EmailConstant.EMAIL_CARS_CREATED_WEEKLY)
                {
                    List<DateTime> dates = Utility.GetDatesInCurrentWeek();
                    //var result = (from firstItem in cars
                    //             join secondItem in dates
                    //             on firstItem.DateCreated equals secondItem.Date
                    //             select firstItem).ToList();
                    cars = cars.Where(i => dates.Contains(i.DateCreated.Date)).ToList();
                    return _emailService.ProcessEmail(EmailConstant.EMAIL_CARS_CREATED_WEEKLY, cars);
                }
                else if (flag == EmailConstant.EMAIL_CARS_CREATED_MONTHLY)
                {
                    List<DateTime> dates = Utility.GetDatesInCurrentMonth();
                    cars = cars.Where(i => dates.Contains(i.DateCreated.Date)).ToList();
                    return _emailService.ProcessEmail(EmailConstant.EMAIL_CARS_CREATED_MONTHLY, cars);
                }
                else
                {
                    return "No Match Found";
                }
                
            }
            catch(Exception e)
            {
                throw;
            }
        }

        public string CarsArchivedReport(int flag)
        {
            try
            {
                List<Car> cars = GetAllCars().ToList();
                if (flag == EmailConstant.EMAIL_CARS_ARCHIVED_TODAY)
                {
                    cars = cars.Where(x => x.DateArchived.Date == DateTime.Today).ToList();
                    return _emailService.ProcessEmail(EmailConstant.EMAIL_CARS_ARCHIVED_TODAY, cars);
                }
                else if (flag == EmailConstant.EMAIL_CARS_ARCHIVED_WEEKLY)
                {
                    List<DateTime> dates = Utility.GetDatesInCurrentWeek();
                    cars = cars.Where(i => dates.Contains(i.DateArchived.Date)).ToList();
                    return _emailService.ProcessEmail(EmailConstant.EMAIL_CARS_ARCHIVED_WEEKLY, cars);
                }
                else if (flag == EmailConstant.EMAIL_CARS_ARCHIVED_MONTHLY)
                {
                    List<DateTime> dates = Utility.GetDatesInCurrentMonth();
                    cars = cars.Where(i => dates.Contains(i.DateArchived.Date)).ToList();
                    return _emailService.ProcessEmail(EmailConstant.EMAIL_CARS_ARCHIVED_MONTHLY, cars);
                }
                else
                {
                    return "No Match Found";
                }

            }
            catch (Exception e)
            {
                throw;
            }
        }

        public IEnumerable<Car> SearchCar(string keyword)
        {
            var list = GetAllCars();
            IEnumerable<Car> result = list.Where(c =>
                                     c.Year.ToString() == keyword ||
                                     c.Make.ToLower().Contains(keyword.ToLower()) ||
                                     c.Model.ToLower().Contains(keyword.ToLower()) ||
                                     c.Badge.ToLower().Contains(keyword.ToLower()) ||
                                     c.EngineSize.ToLower().Contains(keyword.ToLower()) ||
                                     c.Transmission.ToLower().Contains(keyword.ToLower()) ||
                                     c.Dealer.Name.ToLower().Contains(keyword.ToLower()) ||
                                     c.Dealer.Address.ToLower().Contains(keyword.ToLower())
                                     );

            return result;

        }

    }
}
