﻿using CarStockManagement.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarStockManagement.Service.Service
{
    public interface ICarService
    {
        IEnumerable<Car> GetAllCars();
        string CreateCar(Car car);
        bool UpdateCar(Car car);
        bool DeleteCar(Car car);
        string NewCarsReport(int flag);
        string CarsArchivedReport(int flag);
        IEnumerable<Car> SearchCar(string keyword);
    }
}
