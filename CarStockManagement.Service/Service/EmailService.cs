﻿using CarStockManagement.Constants;
using CarStockManagement.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;


namespace CarStockManagement.Service.Service
{
    public class EmailService
    {
        public string ProcessEmail(int emailType, List<Car> cars)
        {
            var bodyBuilder = new StringBuilder();

            switch (emailType)
            {
                case EmailConstant.EMAIL_CARS_CREATED_TODAY:
                    bodyBuilder.AppendLine("Car Stock for Today <br/> <br/>");
                    bodyBuilder.AppendLine("The following cars have been created today. <br/> <br/>");
                    bodyBuilder.AppendLine(BuildEmailBody(cars));
                    break;

                case EmailConstant.EMAIL_CARS_CREATED_WEEKLY:
                    bodyBuilder.AppendLine("Car Stock for this Week <br/> <br/>");
                    bodyBuilder.AppendLine("The following cars have been created this week. <br/> <br/>");
                    bodyBuilder.AppendLine(BuildEmailBody(cars));
                    break;

                case EmailConstant.EMAIL_CARS_CREATED_MONTHLY:
                    bodyBuilder.AppendLine("Car Stock for this Month <br/> <br/>");
                    bodyBuilder.AppendLine("The following cars have been created this month. <br/> <br/>");
                    bodyBuilder.AppendLine(BuildEmailBody(cars));
                    break;
                case EmailConstant.EMAIL_CARS_ARCHIVED_TODAY:
                    bodyBuilder.AppendLine("Cars Archived Today <br/> <br/>");
                    bodyBuilder.AppendLine("The following cars have been archived today. <br/> <br/>");
                    bodyBuilder.AppendLine(BuildEmailBody(cars));
                    break;

                case EmailConstant.EMAIL_CARS_ARCHIVED_WEEKLY:
                    bodyBuilder.AppendLine("Cars Archived this Week <br/> <br/>");
                    bodyBuilder.AppendLine("The following cars have been archived this week. <br/> <br/>");
                    bodyBuilder.AppendLine(BuildEmailBody(cars));
                    break;

                case EmailConstant.EMAIL_CARS_ARCHIVED_MONTHLY:
                    bodyBuilder.AppendLine("Car Archived this Month <br/> <br/>");
                    bodyBuilder.AppendLine("The following cars have been archived this month. <br/> <br/>");
                    bodyBuilder.AppendLine(BuildEmailBody(cars));
                    break;
            }

           
            return bodyBuilder.ToString(); 
        }

        private string BuildEmailBody(List<Car> cars)
        {
            var bodyBuilder = new StringBuilder();
            bodyBuilder.AppendLine("<table> <th> Year </th> <th> Make </th> <th> Model </th> <th> Engine Size </th> <th> Transmission </th> <th> Dealer Name </th> <th> Dealer Email </th> <th> Dealer Address </th>");
            foreach (var car in cars)
            {                
                bodyBuilder.AppendLine("<tr> <td>" + car.Year + "</td> <td>" + car.Make + "</td> <td>" + car.Model + "</td> <td>" + car.EngineSize + "</td> <td>" + car.Transmission + "</td> <td>" + car.Dealer.Name +
                    "</td> <td>" + car.Dealer.Email + "</td> <td>" + car.Dealer.Address + " </td> </tr>");
            }

            bodyBuilder.AppendLine("</table>");

            return bodyBuilder.ToString();
        }

    }
}
