﻿using CarStockManagement.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarStockManagement.Service.Service
{
    interface IDealerService
    {
        IEnumerable<Dealer> GetAllDealers();
        string CreateDealer(Dealer dealer);
        bool UpdateDealer(Dealer dealer);
        bool DeleteDealer(Dealer dealer);

    }
}
