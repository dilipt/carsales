﻿using CarStockManagement.Models.Domain;
using CarStockManagement.Service.DataService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarStockManagement.Service.Service
{
    public class DealerService : IDealerService
    {
        private DealerDataService _dealerDataService;
        
        public DealerService(DealerDataService dealerDataService)
        {
            _dealerDataService = dealerDataService;
        }

        public IEnumerable<Dealer> GetAllDealers()
        {
            try
            {

                var dealers = _dealerDataService.GetAllDealers();

                return dealers;
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public string CreateDealer(Dealer dealer)
        {
            try
            {
                string name = _dealerDataService.CreateDealer(dealer);
                return name;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public bool UpdateDealer(Dealer dealer)
        {
            try
            {
                return _dealerDataService.UpdateDealer(dealer);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public bool DeleteDealer(Dealer dealer)
        {
            try
            {
                return _dealerDataService.DeleteDealer(dealer);
            }
            catch (Exception e)
            {

                throw;
            }
        }

    }
}
