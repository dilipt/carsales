﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarStockManagement.Service.Utilities
{
    public static class Utility
    {
        public static List<DateTime> GetDatesInCurrentWeek()
        {
            DateTime today = DateTime.Today;
            int currentDayOfWeek = (int)today.DayOfWeek;
            DateTime sunday = today.AddDays(-currentDayOfWeek);
            DateTime monday = sunday.AddDays(1);
            if (currentDayOfWeek == 0)
            {
                monday = monday.AddDays(-7);
            }
            List<DateTime> dates = Enumerable.Range(0, 7).Select(days => monday.AddDays(days)).ToList();
            return dates;
        }

        public static List<DateTime> GetDatesInCurrentMonth()
        {
            DateTime today = DateTime.Today;
            return Enumerable.Range(1, DateTime.DaysInMonth(today.Year, today.Month))
                             .Select(day => new DateTime(today.Year, today.Month, day))
                             //.Where(dt => dt.DayOfWeek != DayOfWeek.Sunday &&
                             //             dt.DayOfWeek != DayOfWeek.Saturday)
                             .ToList();
        }
    }
}
