﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarStockManagement.Constants
{
    public static class EmailConstant
    {
        public const int EMAIL_CARS_CREATED_TODAY = 1;
        public const int EMAIL_CARS_CREATED_WEEKLY = 2;
        public const int EMAIL_CARS_CREATED_MONTHLY = 3;

        public const int EMAIL_CARS_ARCHIVED_TODAY = 4;
        public const int EMAIL_CARS_ARCHIVED_WEEKLY = 5;
        public const int EMAIL_CARS_ARCHIVED_MONTHLY = 6;

    }
}