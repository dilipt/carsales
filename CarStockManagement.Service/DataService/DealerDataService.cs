﻿using CarStockManagement.Models.Domain;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CarStockManagement.Service.DataService
{
    public class DealerDataService : IDealerDataService
    {
        string filePath = HttpContext.Current.Server.MapPath("~/dealers.json");

        public IEnumerable<Dealer> GetAllDealers()
        {
            try
            {
                return ReadDealers() as IEnumerable<Dealer>;
            }
            catch (Exception e)
            {
                throw;
            }

        }

        public string CreateDealer(Dealer dealer)
        {
            try
            {
                var list = ReadDealers();
                list.Add(dealer);
                string newJson = JsonConvert.SerializeObject(list);
                File.WriteAllText(filePath, newJson);
                return dealer.Name;
            }
            catch (Exception e)
            {
                throw;
            }

        }

        public bool UpdateDealer(Dealer updatedDealer)
        {
            var list = ReadDealers();
            Dealer dealer = list.First(x => x.Email == updatedDealer.Email);
            var index = list.IndexOf(dealer);

            dealer.Name = updatedDealer.Name;
            dealer.Email = updatedDealer.Email;
            dealer.Address = updatedDealer.Address;

            if (index != -1)
            {
                list[index] = dealer;
                string newJson = JsonConvert.SerializeObject(list);
                File.WriteAllText(filePath, newJson);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DeleteDealer(Dealer dealer)
        {
            var list = ReadDealers();
            Dealer deleteDealer = list.First(x => x.Email == dealer.Email);
            var index = list.IndexOf(deleteDealer);

            if (index != -1)
            {
                list.RemoveAt(index);
                string newJson = JsonConvert.SerializeObject(list);
                File.WriteAllText(filePath, newJson);
                return true;
            }
            return false;
        }

        private List<Dealer> ReadDealers()
        {
            using (StreamReader r = new StreamReader(filePath))
            {
                string json = r.ReadToEnd();
                List<Dealer> dealers = JsonConvert.DeserializeObject<List<Dealer>>(json, new JsonSerializerSettings { DateFormatString = "dd/MM/yyyy HH:mm:ss", NullValueHandling = NullValueHandling.Ignore });
                return dealers;
            }
        }
    }
}
