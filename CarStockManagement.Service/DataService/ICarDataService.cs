﻿using CarStockManagement.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarStockManagement.Service.DataService
{
    public interface ICarDataService
    {
        IEnumerable<Car> GetAllCars();
        string CreateCar(Car car);
        bool UpdateCar(Car car);
        bool DeleteCar(Car car);
       
    }
}
