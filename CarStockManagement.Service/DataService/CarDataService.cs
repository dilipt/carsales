﻿using CarStockManagement.Models.Domain;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CarStockManagement.Service.DataService
{
    public class CarDataService : ICarDataService
    {
        string filePath = HttpContext.Current.Server.MapPath("~/cars.json"); 

        public IEnumerable<Car> GetAllCars()
        {
            try
            {
                return ReadCars() as IEnumerable<Car>;
            }
            catch (Exception e)
            {
                throw;
            }
            
        }

        public string CreateCar(Car car)
        {
            try
            {
                var list = ReadCars();
                list.Add(car);
                string newJson = JsonConvert.SerializeObject(list);
                File.WriteAllText(filePath, newJson);
                return car.Model;
            }
            catch(Exception e)
            {
                throw;
            }
            
        }

        public bool UpdateCar(Car updatedCar)
        {
            var list = ReadCars();
            Car car = list.First(x => x.Year == updatedCar.Year && x.Make == updatedCar.Make && x.Model == updatedCar.Model);
            var index = list.IndexOf(car);

            car.Year = updatedCar.Year;
            car.Make = updatedCar.Make;
            car.Model = updatedCar.Model;
            car.Badge = updatedCar.Badge;
            car.EngineSize = updatedCar.EngineSize;
            car.Transmission = updatedCar.Transmission;
            car.Dealer = updatedCar.Dealer;
            car.DateModified = DateTime.Now;

            if (index != -1)
            {
                list[index] = car;
                string newJson = JsonConvert.SerializeObject(list);
                File.WriteAllText(filePath, newJson);
                return true;
            }
            else
            {
                return false;
            }
           

        }

        public bool DeleteCar(Car car)
        {
            var list = ReadCars();
            Car deleteCar = list.First(x => x.Year == car.Year && x.Make == car.Make && x.Model == car.Model);
            var index = list.IndexOf(deleteCar);

            if (index != -1)
            {
                list.RemoveAt(index);
                string newJson = JsonConvert.SerializeObject(list);
                File.WriteAllText(filePath, newJson);
                return true;
            }

            return false;

        }

       

        private List<Car> ReadCars()
        {
            using (StreamReader r = new StreamReader(filePath))
            {
                string json = r.ReadToEnd();
                List<Car> cars = JsonConvert.DeserializeObject<List<Car>>(json, new JsonSerializerSettings { DateFormatString = "dd/MM/yyyy HH:mm:ss", NullValueHandling = NullValueHandling.Ignore });
                return cars;
            }
        }
    }
}
