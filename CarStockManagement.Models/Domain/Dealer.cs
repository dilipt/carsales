﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarStockManagement.Models.Domain
{
    public class Dealer
    {
        //mandatory
        public string Name { get; set; }

        //mandatory
        public string Email { get; set; }

        public string Address { get; set; }

    }
}
