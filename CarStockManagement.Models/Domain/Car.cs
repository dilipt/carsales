﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarStockManagement.Models.Domain
{
    public class Car
    {
        //mandatory
        public int Year { get; set; }

        //mandatory
        public string Make { get; set; }

        //mandatory
        public string Model { get; set; }

        public string Badge { get; set; }

        public string EngineSize { get; set; }

        //mandatory
        public string Transmission { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateModified { get; set; }
        public DateTime DateArchived { get; set; }

        //mandatory
        public Dealer Dealer { get; set; }

    }

}
