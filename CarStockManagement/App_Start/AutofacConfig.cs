﻿using Autofac;
using Autofac.Integration.WebApi;
using CarStockManagement.Service.DataService;
using CarStockManagement.Service.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;

namespace CarStockManagement.App_Start
{
    public class AutofacConfig
    {
        public static void Configure()
        {
            var builder = new ContainerBuilder();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterType<CarService>().AsSelf().InstancePerRequest();
            builder.RegisterType<DealerService>().AsSelf().InstancePerRequest();
            builder.RegisterType<CarDataService>().AsSelf().InstancePerRequest();
            builder.RegisterType<DealerDataService>().AsSelf().InstancePerRequest();
            builder.RegisterType<EmailService>().AsSelf().InstancePerRequest();

            var container = builder.Build();
            var resolver = new AutofacWebApiDependencyResolver(container);
            GlobalConfiguration.Configuration.DependencyResolver = resolver;
        }
    }
}