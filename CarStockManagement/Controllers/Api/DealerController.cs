﻿using CarStockManagement.Models.Domain;
using CarStockManagement.Service.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CarStockManagement.Controllers.Api
{
    public class DealerController : ApiController
    {
        private DealerService _dealerService;

        public DealerController(DealerService dealerService)
        {
            _dealerService = dealerService;
        }

        [Route("api/dealers/")]
        [HttpGet]
        public HttpResponseMessage GetAllDealers()
        {
            try
            {
                var dealers = _dealerService.GetAllDealers();
                return Request.CreateResponse(HttpStatusCode.OK, dealers);

            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        [Route("api/new-dealer/")]
        [HttpPost]
        public HttpResponseMessage CreateDealer(Dealer dealer)
        {
            try
            {
                string result = _dealerService.CreateDealer(dealer);

                if (!string.IsNullOrEmpty(result))
                {
                    return Request.CreateResponse(HttpStatusCode.Created);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                }
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        [Route("api/update-dealer/")]
        [HttpPut]
        public HttpResponseMessage UpdateDealer(Dealer dealer)
        {
            try
            {
                bool result = _dealerService.UpdateDealer(dealer);
                if (result)
                {
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                }


            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        [Route("api/delete-dealer/")]
        [HttpDelete]
        public HttpResponseMessage DeleteDealer(Dealer dealer)
        {
            try
            {
                bool result = _dealerService.DeleteDealer(dealer);
                if (result)
                {
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                }


            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

    }
}
