﻿using CarStockManagement.Models.Domain;
using CarStockManagement.Service.Service;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace CarStockManagement.Controllers.Api
{
    public class CarController : ApiController
    {
        private CarService _carService;

        public CarController(CarService carService)
        {
            _carService = carService;
        }

        [Route("api/cars/")]
        [HttpGet]
        public HttpResponseMessage GetAllCars()
        {
            try
            {
                var cars = _carService.GetAllCars();
                return Request.CreateResponse(HttpStatusCode.OK, cars);

            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        [Route("api/new-car/")]
        [HttpPost]
        public HttpResponseMessage CreateCar(Car car)
        {
            try
            {
                string result = _carService.CreateCar(car);

                if (!string.IsNullOrEmpty(result))
                {
                    return Request.CreateResponse(HttpStatusCode.Created);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                } 
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        [Route("api/update-car/")]
        [HttpPut]
        public HttpResponseMessage UpdateCar(Car car)
        {
            try
            {
              bool result = _carService.UpdateCar(car);
              if (result)
              {
                    return Request.CreateResponse(HttpStatusCode.NoContent);
              }
              else
              {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
              }               
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        [Route("api/delete-car/")]
        [HttpDelete]
        public HttpResponseMessage DeleteCar(Car car)
        {
            try
            {
                bool result = _carService.DeleteCar(car);
                if(result)
                {
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                }               
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        [Route("api/new-car-report/{flag}")]
        [HttpGet]
        public HttpResponseMessage NewCarsReport(int flag)
        {
            try
            {
                var result = _carService.NewCarsReport(flag);
                var response = new HttpResponseMessage();
                response.Content = new StringContent(result);
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
                return response;
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        [Route("api/car-archive-report/{flag}")]
        [HttpGet]
        public HttpResponseMessage CarsArchivedReport(int flag)
        {
            try
            {
                var result = _carService.CarsArchivedReport(flag);
                var response = new HttpResponseMessage();
                response.Content = new StringContent(result);
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
                return response;
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        [Route("api/search-cars/{keyword}")]
        [HttpGet]
        public HttpResponseMessage SearchCar(string keyword)
        {
            try
            {
                var result = _carService.SearchCar(keyword);
                if (result != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                }


            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
    }
}
